const path = require('path');
const NodemonPlugin = require('nodemon-webpack-plugin') // Ding
const SRC_DIR = path.resolve(__dirname, 'server');
const DIST_DIR = path.resolve(__dirname, 'dist');

const nodemon = new NodemonPlugin({
    /// Arguments to pass to the script being watched.
    args: ['demo'],

    // What to watch.
    watch: path.resolve('./dist'),

    // Files to ignore.
    ignore: ['*.js.map'],

    // Detailed log.
    verbose: true,

    // Node arguments.
    nodeArgs: ['--inspect=9222'],

    // If using more than one entry, you can specify
    // which output file will be restarted.
    script: './dist/server.js',

    // Extensions to watch
    ext: 'js,njk,json'
});

var fs = require('fs');
var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function (x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function (mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    entry: './server/server.js',
    output: {
        path: __dirname + '/dist',
        filename: 'server.js',
    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: ['.js'],
    },
    module: {
        rules: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                },
                exclude: [
                    path.resolve(__dirname, 'node_modules'),
                ]
            },
        ]
    },
    plugins: [
        nodemon
    ],
    target: 'node',
    externals: nodeModules,
};