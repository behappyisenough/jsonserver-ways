var fs = require('fs')
var _ = require('underscore')
var Factory = require('rosie').Factory
var Faker = require('Faker')
var db = {}

// Credit http://www.paulirish.com/2009/random-hex-color-code-snippets/
function hex() {
    return Math.floor(Math.random() * 16777215).toString(16)
}

// Tables
db.users = []


Factory.define('user')
    .sequence('id')
    .after(function (user) {
        var card = Faker.Helpers.userCard()
        _.each(card, function (value, key) {
            user[key] = value
        })
    })

// Has many relationships
// Users
_(10).times(function () {
    var user = Factory.build('user')
    db.users.push(user)
})

fs.writeFileSync('server/db.json', JSON.stringify(db, null, 2));