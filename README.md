# Commands

```
npm run start
npm run generate
npm run build
```

# Refs

- https://github.com/typicode/jsonplaceholder
- https://github.com/typicode/json-server

# Dev

```

```

# Deploy to stage

```
heroku login
```

**First time**

```
heroku git:remote -a restapiways
```

**Check git**

```
git remote -v
```

**Deploy**

```
git add .
git commit -am "First deploy"
git push heroku master
```

```
npm run deploy:stage
npm run open:stage
```