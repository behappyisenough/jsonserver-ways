var shell = require('shelljs');
 
if (!shell.which('heroku')) {
  shell.echo('Sorry, this script requires heroku');
  shell.exit(1);
}
 
// Run external tool synchronously
if (shell.exec('heroku open"').code !== 0) {
  shell.echo('Error: heroku open failed');
  shell.exit(1);
}