const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('./db.json')
const middlewares = jsonServer.defaults()
var port = process.env.PORT || 3000
// rewriter
server.use(jsonServer.rewriter({
  '/api/*': '/$1'
}))

server.use(middlewares)
server.use(router)
server.listen(port, () => {
  console.log('JSON Server is running at http://localhost:%s/api', port);
})